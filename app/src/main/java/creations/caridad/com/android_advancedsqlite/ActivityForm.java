package creations.caridad.com.android_advancedsqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import creations.caridad.com.android_advancedsqlite.SQLite.SQLiteHelper;
import creations.caridad.com.android_advancedsqlite.usersclass.ClassUsers;

public class ActivityForm extends AppCompatActivity {

    Context context;
   // ArrayList<ClassUsers> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        context = this;

        // call methods here
        saveNewUser();
    }

    private void saveNewUser() {
        // XML UI
        final EditText enterFirstName = (EditText) findViewById(R.id.editTextFirstName);
        final EditText enterLastName = (EditText) findViewById(R.id.editTextLastName);
        final EditText enterAgeName = (EditText) findViewById(R.id.editTextage);
        Button buttonSave = (Button) findViewById(R.id.buttonSave);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if edit text fields are empty
                if (enterFirstName.getText().toString().trim().length() != 0 && enterLastName.getText().toString().trim().length() != 0 && enterAgeName.getText().toString().trim().length() != 0) {
                    //  add data to class arraylist
                  //  arrayList.add(new ClassUsers(enterFirstName.getText().toString(), enterLastName.getText().toString(), enterAgeName.getText().toString()));
                    // instance of our helper
                    SQLiteHelper insert = new SQLiteHelper();
                    // pass data to method
                    insert.createData(context, enterFirstName.getText().toString(), enterLastName.getText().toString(), enterAgeName.getText().toString());
                    // send message
                    Toast.makeText(context, "Save Successful", Toast.LENGTH_SHORT).show();
                    // go back
                    finish();
                } else {
                    Toast.makeText(context, "Please fill everything out", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
