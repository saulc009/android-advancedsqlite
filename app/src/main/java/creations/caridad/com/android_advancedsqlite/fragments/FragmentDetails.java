package creations.caridad.com.android_advancedsqlite.fragments;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import creations.caridad.com.android_advancedsqlite.R;
import creations.caridad.com.android_advancedsqlite.SQLite.DatabaseHelper;
import creations.caridad.com.android_advancedsqlite.SQLite.SQLiteHelper;


public class FragmentDetails extends Fragment {

    public static final String TAG = "FragmentDetails";
    public static final String userIndex = "userfirstname";

    SQLiteDatabase sqLiteDatabase;

    // XML UI
    TextView textViewFirstName;
    TextView textViewLastName;
    TextView textViewAge;

    Button buttonUpdate;
    Button buttonDelete;


    // instance will handle storing index into bundle
    public static FragmentDetails newInstance(int index) {
        FragmentDetails fragmentDetails = new FragmentDetails();
        // stores index in bundle
        Bundle bundle = new Bundle();
        bundle.putInt(userIndex, index);
        fragmentDetails.setArguments(bundle);
        return fragmentDetails;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        // find ui
        textViewFirstName = (EditText) view.findViewById(R.id.editTextDetailFirstName);
        textViewLastName = (EditText) view.findViewById(R.id.editTextDetailLastName);
        textViewAge = (EditText) view.findViewById(R.id.editTextDetailage);

        buttonDelete = (Button) view.findViewById(R.id.buttonDetailDelete);
        buttonUpdate = (Button) view.findViewById(R.id.buttonDetailUpdate);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // find our DB
        sqLiteDatabase = new DatabaseHelper(getContext()).getWritableDatabase();

        if (getArguments() != null) {
            // call methods here
            deleteAtIndex(getArguments().getInt(userIndex));
            displayCurrentDataAtIndex(getArguments().getInt(userIndex));
            updateAtIndex(getArguments().getInt(userIndex));
        }
    }

    private void deleteAtIndex(final int index) {
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // have our helper delete the data from our DB
                SQLiteHelper sqLiteHelper = new SQLiteHelper();
                // pass where to delete with index
                sqLiteHelper.deleteData(getContext(), index, 1);
                // send message
                Toast.makeText(getContext(), "Delete Success", Toast.LENGTH_SHORT).show();
                // go back
                getFragmentManager().popBackStack();
            }
        });
    }

    private void displayCurrentDataAtIndex(int index) {
        // find db cursor
        Cursor cursor = sqLiteDatabase.query(DatabaseHelper.TABLENAME, null, null, null, null, null, null);
        // moce cursor to top of db
        cursor.moveToPosition(index);
        if (cursor != null || cursor.getCount() != 0) {
            // set text
            textViewFirstName.setText(cursor.getString(1));
            textViewLastName.setText(cursor.getString(2));
            textViewAge.setText(cursor.getString(3));
        }
    }

    private void updateAtIndex(final int index) {
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check for empty textview
                if (textViewFirstName.getText().toString().trim().length() != 0 && textViewLastName.getText().toString().trim().length() != 0 && textViewAge.getText().toString().trim().length() != 0) {
                    SQLiteHelper sqLiteHelper = new SQLiteHelper();
                    sqLiteHelper.updateData(getContext(), index, textViewFirstName.getText().toString(), textViewLastName.getText().toString(), textViewAge.getText().toString());
                    // send message
                    Toast.makeText(getContext(), "Update Success", Toast.LENGTH_SHORT).show();
                    // go back
                    getFragmentManager().popBackStack();
                }
            }
        });
    }
}