package creations.caridad.com.android_advancedsqlite.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Saul on 6/8/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASEFILE = "database.db";
    public static final int DATABASEVERSION = 1;

    public static final String TABLENAME = "users";
    public static final String ID = "_id";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String AGE = "age";

    private static final String CREATETABLE = "CREATE TABLE IF NOT EXISTS " + TABLENAME + " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + FIRSTNAME + " TEXT, " + LASTNAME + " TEXT, " + AGE + " TEXT)";

    // constructor
    public DatabaseHelper(Context context) {
        super(context, DATABASEFILE, null, DATABASEVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // EXECUTE
        db.execSQL(CREATETABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // HANDLE UPDATE
    }
}
