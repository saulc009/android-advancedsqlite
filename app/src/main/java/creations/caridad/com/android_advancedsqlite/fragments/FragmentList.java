package creations.caridad.com.android_advancedsqlite.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import creations.caridad.com.android_advancedsqlite.R;
import creations.caridad.com.android_advancedsqlite.SQLite.SQLiteHelper;


public class FragmentList extends ListFragment {

    public static final String TAG = "FragmentList";

    // create interface
    public interface InterfaceListView {
        void passDataToNextActivity(int index);
    }

    // instance of interface
    private InterfaceListView interfaceListView;


    // attach interface
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InterfaceListView) {
            interfaceListView = (InterfaceListView) context;
        } else {
            Log.i(TAG, "ERROR : Can't connect interface");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // call methods here
        handleListView();
    }

    @Override
    public void onResume() {
        super.onResume();
        // anything that needs refreshing
        handleListView();
    }

    private void handleListView() {
        // set new class method
        SQLiteHelper load = new SQLiteHelper();
        // set method from SqLite_Helpers and set to listview
        setListAdapter(load.readDB(getContext()));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        // passing listview index to activityMain
        interfaceListView.passDataToNextActivity(position);
    }
}