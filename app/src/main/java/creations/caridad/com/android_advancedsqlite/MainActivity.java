package creations.caridad.com.android_advancedsqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import creations.caridad.com.android_advancedsqlite.fragments.FragmentDetails;
import creations.caridad.com.android_advancedsqlite.fragments.FragmentList;

public class MainActivity extends AppCompatActivity implements FragmentList.InterfaceListView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // call methods here
        setUpFragmentList();
    }

    private void setUpFragmentList() {
        FragmentList fragmentList = (FragmentList) getSupportFragmentManager().findFragmentByTag(FragmentList.TAG);
        if (fragmentList == null) {
            fragmentList = new FragmentList();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.containerFragmentList, fragmentList, FragmentList.TAG).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // setup menu to activity
        getMenuInflater().inflate(R.menu.menu_activitymain, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuButtonSave:
                // explicit intent
                Intent intent = new Intent(this, ActivityForm.class);
                startActivity(intent);
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public void passDataToNextActivity(int index) {
        // find fragment by tag
        FragmentDetails fragmentDetails = (FragmentDetails) getSupportFragmentManager().findFragmentByTag(FragmentDetails.TAG);
        if (fragmentDetails == null) {
            fragmentDetails = FragmentDetails.newInstance(index);
        }
        //assign to container
        getSupportFragmentManager().beginTransaction().addToBackStack(FragmentDetails.TAG).replace(R.id.containerFragmentList, fragmentDetails, FragmentDetails.TAG).commit();
    }
}
