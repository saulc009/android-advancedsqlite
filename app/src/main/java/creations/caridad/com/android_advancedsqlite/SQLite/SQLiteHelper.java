package creations.caridad.com.android_advancedsqlite.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Saul on 6/8/17.
 */

public class SQLiteHelper {
    // Create instance of DB
    SQLiteDatabase database;

    // Instance of our Custom cursor
    CustomCursorAdapter customCursorAdapter;

    // Find
    // method returns cursor adapter which call the DB to return our data
    public CustomCursorAdapter readDB(Context context) {
        // find our DB
        database = new DatabaseHelper(context).getWritableDatabase();
        // create our cursor
        Cursor cursor = getCursor(database);
        // get cursor adapter which passes context and cursor
        customCursorAdapter = new CustomCursorAdapter(context, cursor);
        // cursor
        return customCursorAdapter;
    }

    // getting DB data
    public Cursor getCursor(SQLiteDatabase database) {
        return database.query(DatabaseHelper.TABLENAME, null, null, null, null, null, null);
    }

    // Add
    // handles creating data in our DB
    public void createData(Context context, String firstName, String lastName, String age) {
        // find our DB
        database = new DatabaseHelper(context).getWritableDatabase();
        // create object to insert key/value pairs
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.FIRSTNAME, firstName);
        contentValues.put(DatabaseHelper.LASTNAME, lastName);
        contentValues.put(DatabaseHelper.AGE, age);
        // insert into DB
        database.insert(DatabaseHelper.TABLENAME, null, contentValues);
    }

    // Update
    // handle updating a row in our DB
    public void updateData(Context context, int indexToUpdate, String firstName, String lastName, String age) {
        // find our db
        database = new DatabaseHelper(context).getWritableDatabase();

        // create object to insert key/value pairs
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.FIRSTNAME, firstName);
        contentValues.put(DatabaseHelper.LASTNAME, lastName);
        contentValues.put(DatabaseHelper.AGE, age);

        // create our cursor
        Cursor cursor = getCursor(database);
        // move our cursor to the right index
        cursor.moveToPosition(indexToUpdate);
        // adding a plus 1 since our listview index starts at 0 and SQLite row starts at 1.
        indexToUpdate = indexToUpdate + 1;
        // where to update.
        String where = DatabaseHelper.ID + "=" + indexToUpdate + "" + "";
        // update
        database.update(DatabaseHelper.TABLENAME, contentValues, where, null);
    }


    public void deleteData(Context context, int indexOfFieldToDelete, int whatFieldToDelete) {
        // find our DB
        database = new DatabaseHelper(context).getWritableDatabase();
        // create our cursor
        Cursor cursor = getCursor(database);
        // where to delete
        cursor.moveToPosition(indexOfFieldToDelete);
        // grabs index of the property location
        String[] whereToDelete = {String.valueOf(cursor.getString(whatFieldToDelete))};
        // Get name
        String where = DatabaseHelper.FIRSTNAME + "=?";
        // delete
        database.delete(DatabaseHelper.TABLENAME, where, whereToDelete);
    }
}