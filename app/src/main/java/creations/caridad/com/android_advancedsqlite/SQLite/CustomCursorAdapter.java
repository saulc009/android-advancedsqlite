package creations.caridad.com.android_advancedsqlite.SQLite;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import creations.caridad.com.android_advancedsqlite.R;


public class CustomCursorAdapter extends ResourceCursorAdapter {

    // will set cursor in our DB
    public CustomCursorAdapter(Context context, Cursor cursor) {
        super(context, R.layout.support_simple_spinner_dropdown_item, cursor, 0);
    }

    // binds our XML view
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String firstNameTitle = cursor.getString(1);
        ((TextView) view).setText(firstNameTitle);
    }
}