package creations.caridad.com.android_advancedsqlite.usersclass;


public class ClassUsers {

    String firstName;
    String lastName;
    String age;

    // constructor
    public ClassUsers(String firstName, String lastName, String age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    // toString
    @Override
    public String toString() {
        return firstName;
    }

    // getters
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAge() {
        return age;
    }
}
