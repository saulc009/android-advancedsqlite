# README #

This app demonstrates advanced SQLite methods. It uses all CRUD using SQLite.

### SQLite ###
* Implements Create, Read, Update, Delete

### Other Features ###
* Uses Interfaces
* ListFragment
* ResourceCursorAdapter
* Cursor Adapter bindview
* Intents to start next activity
* Fragments
* Bundle to pass data within fragment
* ContentValues for SQLite

### Official Documentation ###

* [SQLite](https://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html)

* [Fragment Interface](https://developer.android.com/training/basics/fragments/communicating.html)

* [ListFragment](https://developer.android.com/reference/android/app/ListFragment.html)

* [ResourceCursorAdapter](https://developer.android.com/reference/android/widget/ResourceCursorAdapter.html)

* [Cursor Adapter bindview](https://developer.android.com/reference/android/widget/CursorAdapter.html)

* [Intents](https://developer.android.com/reference/android/content/Intent.html)

* [Fragments](https://developer.android.com/guide/components/fragments.html)

* [Bundle](https://developer.android.com/reference/android/os/Bundle.html)

* [ContentValues](https://developer.android.com/reference/android/content/ContentValues.html)